from matplotlib import pyplot as plt


def count_p0(prob_coeffs, epsilon, n_channels, lamda, mu, nu):
    counter = 0
    for i in range(1, n_channels + 1):
        prob_coeffs.append(prob_coeffs[i - 1] * lamda / (i * mu))
    p0 = 1 / sum(prob_coeffs)
    p0_prev = 1 / sum(prob_coeffs[:-1])

    while abs(p0 - p0_prev) >= epsilon:
        counter += 1
        prob_coeffs.append(prob_coeffs[-1] * (lamda / (n_channels * mu + counter * nu)))
        p0_prev = p0
        p0 = 1 / sum(prob_coeffs)
    return p0, counter + 1


def count_n_avg(prob_coeffs, p0, channels_num, q_elems_num):
    n_sum = 0
    for i in range(channels_num + 1):
        n_sum += i * prob_coeffs[i]
    for i in range(q_elems_num):
        n_sum += channels_num * prob_coeffs[channels_num + i]
    return n_sum * p0


def count_p_queue(prob_coeffs, epsilon, n_channels, lamda, p0, mu, nu):
    p_queue = 0
    for i in range(n_channels, len(prob_coeffs)):
        p_queue += prob_coeffs[i]
    return p_queue * p0


def count_q_avg(prob_coeffs, epsilon, n_channels, lamda, p0, mu, nu):
    p_queue = 0
    for i in range(n_channels, len(prob_coeffs)):
        p_queue += (i - n_channels) * prob_coeffs[i]
    return p_queue * p0


def main():
    coming_time = 26
    service_time = 231
    wait_time = 517
    epsilon = 1e-6

    lamda = 1 / coming_time
    mu = 1 / service_time
    nu = 1 / wait_time
    n_avgs = []
    busy_coeffs = []
    q_avgs = []
    p_queues = []
    max_channel_num = 17
    channel_nums = []
    for channel_num in range(1, max_channel_num + 1):
        prob_coeffs = [1.]
        channel_nums.append(channel_num)
        p0, q_elems_num = count_p0(prob_coeffs, epsilon, channel_num, lamda, mu, nu)

        n_avg = count_n_avg(prob_coeffs, p0, channel_num, q_elems_num)
        n_avgs.append(n_avg)
        busy_coeffs.append(n_avg / channel_num)
        q_avgs.append(count_q_avg(prob_coeffs, epsilon, channel_num, lamda, p0, mu, nu))
        p_queues.append(count_p_queue(prob_coeffs, epsilon, channel_num, lamda, p0, mu, nu))
        channel_num += 1

    plt.plot(channel_nums, n_avgs, '-')
    plt.grid()
    plt.ylabel('N avg')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, busy_coeffs, '-', )
    plt.grid()
    plt.ylabel('K busy')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, p_queues, '-', )
    plt.grid()
    plt.ylabel('Q probability')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, q_avgs, '-', )
    plt.grid()
    plt.ylabel('Q avg')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()


if __name__ == '__main__':
    main()
