from matplotlib import pyplot as plt


def main():
    coming_time = 26
    service_time = 231

    lamda = 1 / coming_time
    mu = 1 / service_time
    p_cancels = []
    n_avgs = []
    busy_coeffs = []
    stop = False
    channel_num = 1
    channel_nums = []
    while not stop:
        prob_coeffs = [1.]
        for i in range(1, channel_num + 1):
            prob_coeffs.append(prob_coeffs[i - 1] * lamda / (i * mu))

        channel_nums.append(channel_num)
        p0 = 1 / sum(prob_coeffs)

        p_cancel = p0 * prob_coeffs[-1]
        p_cancels.append(p_cancel)

        n_sum = 0
        for i in range(0, channel_num + 1):
            n_sum += i * prob_coeffs[i]
        n_avgs.append(n_sum * p0)
        busy_coeffs.append((n_sum * p0 / channel_num))

        if p_cancel <= 0.01:
            stop = True
        channel_num += 1

    plt.plot(channel_nums, p_cancels, '-')
    plt.grid()
    plt.ylabel('P cancel')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, n_avgs, '-')
    plt.grid()
    plt.ylabel('N avg')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, busy_coeffs, '-', )
    plt.grid()
    plt.ylabel('K busy')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()


if __name__ == '__main__':
    main()
