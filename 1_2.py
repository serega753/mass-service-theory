from matplotlib import pyplot as plt
import numpy as np


def main_operators():
    coming_time = 26
    service_time = 231

    lamda = 1 / coming_time
    mu = 1 / service_time
    p_cancels = []
    n_avgs = []
    q_avgs = []
    busy_coeffs = []
    queue_busy_coeffs = []
    p_queues = []
    channel_num = 5  # we vary this, maximum is 17
    max_q_len = 15
    for queue_len in range(1, max_q_len + 1):
        prob_coeffs = [1.]
        for i in range(1, channel_num + 1):
            prob_coeffs.append(prob_coeffs[i - 1] * lamda / (i * mu))
        for i in range(channel_num + 1, channel_num + queue_len):
            prob_coeffs.append(prob_coeffs[i - 1] * lamda / (channel_num * mu))

        p0 = 1 / sum(prob_coeffs)

        p_cancel = p0 * prob_coeffs[-1]
        p_cancels.append(p_cancel)

        n_sum = 0
        q_sum = 0
        for i in range(0, channel_num + 1):
            n_sum += i * prob_coeffs[i]
        for i in range(channel_num + 1, channel_num + queue_len):
            n_sum += channel_num * prob_coeffs[i]
            q_sum += (i - channel_num) * prob_coeffs[i]
        n_avgs.append(n_sum * p0)
        busy_coeffs.append((n_sum * p0 / channel_num))
        p_queues.append(sum(prob_coeffs[channel_num:]) * p0)
        q_avgs.append(q_sum * p0)
        queue_busy_coeffs.append(q_sum * p0 / queue_len)

    plt.plot([i for i in range(1, max_q_len + 1)], p_cancels, '-')
    plt.grid()
    plt.ylabel('P cancel')
    plt.xlabel('Queue length')
    plt.xticks([i for i in range(1, max_q_len + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_q_len + 1)], n_avgs, '-')
    plt.grid()
    plt.ylabel('N avg')
    plt.xlabel('Queue length')
    plt.xticks([i for i in range(1, max_q_len + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_q_len + 1)], busy_coeffs, '-', )
    plt.grid()
    plt.ylabel('K busy')
    plt.xlabel('Queue length')
    plt.xticks([i for i in range(1, max_q_len + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_q_len + 1)], p_queues, '-')
    plt.grid()
    plt.ylabel('P queue')
    plt.xlabel('Queue length')
    plt.xticks([i for i in range(1, max_q_len + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_q_len + 1)], q_avgs, '-')
    plt.grid()
    plt.ylabel('Q avg')
    plt.xlabel('Queue length')
    plt.xticks([i for i in range(1, max_q_len + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_q_len + 1)], queue_busy_coeffs, '-')
    plt.grid()
    plt.ylabel('K queue busy')
    plt.xlabel('Queue length')
    plt.xticks([i for i in range(1, max_q_len + 1)])
    plt.show()


def main_queue():
    coming_time = 26
    service_time = 231

    lamda = 1 / coming_time
    mu = 1 / service_time
    p_cancels = []
    n_avgs = []
    q_avgs = []
    busy_coeffs = []
    queue_busy_coeffs = []
    p_queues = []
    queue_len = 10  # we vary this
    max_channel_num = 15
    for channel_num in range(1, max_channel_num + 1):
        prob_coeffs = [1.]
        for i in range(1, channel_num + 1):
            prob_coeffs.append(prob_coeffs[i - 1] * lamda / (i * mu))
        for i in range(channel_num + 1, channel_num + queue_len):
            prob_coeffs.append(prob_coeffs[i - 1] * lamda / (channel_num * mu))

        p0 = 1 / sum(prob_coeffs)

        p_cancel = p0 * prob_coeffs[-1]
        p_cancels.append(p_cancel)

        n_sum = 0
        q_sum = 0
        for i in range(0, channel_num + 1):
            n_sum += i * prob_coeffs[i]
        for i in range(channel_num + 1, channel_num + queue_len):
            n_sum += channel_num * prob_coeffs[i]
            q_sum += (i - channel_num) * prob_coeffs[i]
        n_avgs.append(n_sum * p0)
        busy_coeffs.append((n_sum * p0 / channel_num))
        p_queues.append(sum(prob_coeffs[channel_num:]) * p0)
        q_avgs.append(q_sum * p0)
        queue_busy_coeffs.append(q_sum * p0 / queue_len)

    plt.plot([i for i in range(1, max_channel_num + 1)], p_cancels, '-')
    plt.grid()
    plt.ylabel('P cancel')
    plt.xlabel('Operators number')
    plt.xticks([i for i in range(1, max_channel_num + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_channel_num + 1)], n_avgs, '-')
    plt.grid()
    plt.ylabel('N avg')
    plt.xlabel('Operators number')
    plt.xticks([i for i in range(1, max_channel_num + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_channel_num + 1)], busy_coeffs, '-', )
    plt.grid()
    plt.ylabel('K busy')
    plt.xlabel('Operators number')
    plt.xticks([i for i in range(1, max_channel_num + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_channel_num + 1)], p_queues, '-')
    plt.grid()
    plt.ylabel('P queue')
    plt.xlabel('Operators number')
    plt.xticks([i for i in range(1, max_channel_num + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_channel_num + 1)], q_avgs, '-')
    plt.grid()
    plt.ylabel('Q avg')
    plt.xlabel('Operators number')
    plt.xticks([i for i in range(1, max_channel_num + 1)])
    plt.show()

    plt.plot([i for i in range(1, max_channel_num + 1)], queue_busy_coeffs, '-')
    plt.grid()
    plt.ylabel('K queue busy')
    plt.xlabel('Operators number')
    plt.xticks([i for i in range(1, max_channel_num + 1)])
    plt.show()


if __name__ == '__main__':
    # main_operators()
    main_queue()
