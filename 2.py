from matplotlib import pyplot as plt


def main():
    coming_time = 297
    service_time = 11
    tools_num = 38

    lamda = 1 / coming_time
    mu = 1 / service_time
    not_workings = []
    waitings = []
    p_waitings = []
    busy_workers = []
    busy_workers_coeffs = []
    channel_nums = [i for i in range(1, tools_num)]
    for channel_num in channel_nums:
        prob_coeffs = [1.]
        for i in range(1, channel_num + 1):
            multiplier = (tools_num - i) * lamda / (i * mu)
            prob_coeffs.append(prob_coeffs[i - 1] * multiplier)
        for i in range(channel_num + 1, tools_num + 1):
            multiplier = (tools_num - i) * lamda / (channel_num * mu)
            prob_coeffs.append(prob_coeffs[i - 1] * multiplier)
        p0 = 1 / (sum(prob_coeffs))

        n_sum = 0
        for i in range(0, tools_num + 1):
            n_sum += i * prob_coeffs[i]
        not_workings.append(n_sum * p0)

        n_sum = 0
        for i in range(channel_num + 1, tools_num + 1):
            n_sum += i * prob_coeffs[i]
        waitings.append(n_sum * p0)

        p_waitings.append(sum(prob_coeffs[channel_num + 1:]) * p0)

        n_sum = 0
        for i in range(channel_num + 1):
            n_sum += i * prob_coeffs[i]
        for i in range(channel_num + 1, tools_num + 1):
            n_sum += channel_num * prob_coeffs[i]
        busy_workers.append(n_sum * p0)

        busy_workers_coeffs.append(n_sum * p0 / channel_num)

    plt.plot(channel_nums, not_workings, '-')
    plt.grid()
    plt.ylabel('N not working')
    plt.xlabel('N operator')
    # plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, waitings, '-', )
    plt.grid()
    plt.ylabel('N waiting')
    plt.xlabel('N operator')
    # plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, p_waitings, '-', )
    plt.grid()
    plt.ylabel('Waiting probability')
    plt.xlabel('N operator')
    # plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, busy_workers, '-', )
    plt.grid()
    plt.ylabel('N busy workers')
    plt.xlabel('N operator')
    # plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, busy_workers_coeffs, '-', )
    plt.grid()
    plt.ylabel('K busy of workers')
    plt.xlabel('N operator')
    # plt.xticks(channel_nums)
    plt.show()


if __name__ == '__main__':
    main()
