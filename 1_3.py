from matplotlib import pyplot as plt


def main():
    coming_time = 26
    service_time = 231

    lamda = 1 / coming_time
    mu = 1 / service_time
    n_avgs = []
    busy_coeffs = []
    q_avgs = []
    p_queues = []
    max_channel_num = 17
    channel_nums = []
    for channel_num in range(10, max_channel_num + 1):
        prob_coeffs = [1.]
        for i in range(1, channel_num + 1):
            prob_coeffs.append(prob_coeffs[i - 1] * lamda / (i * mu))
        a = lamda / (channel_num * mu)
        channel_nums.append(channel_num)
        p0 = 1 / (sum(prob_coeffs) + (a * prob_coeffs[-1]) / (1 - a))

        n_sum = 0
        for i in range(0, channel_num + 1):
            n_sum += i * prob_coeffs[i]
        n_sum += prob_coeffs[-1] * channel_num * a / (1 - a)
        n_avgs.append(n_sum * p0)
        busy_coeffs.append((n_sum * p0 / channel_num))
        q_avgs.append((a * p0 * prob_coeffs[-1]) / ((1 - a) ** 2))
        p_queues.append((a * p0 * prob_coeffs[-1]) / (1 - a))
        channel_num += 1

    plt.plot(channel_nums, n_avgs, '-')
    plt.grid()
    plt.ylabel('N avg')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, busy_coeffs, '-', )
    plt.grid()
    plt.ylabel('K busy')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, p_queues, '-', )
    plt.grid()
    plt.ylabel('Q probability')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()

    plt.plot(channel_nums, q_avgs, '-', )
    plt.grid()
    plt.ylabel('Q avg')
    plt.xlabel('N operator')
    plt.xticks(channel_nums)
    plt.show()


if __name__ == '__main__':
    main()
